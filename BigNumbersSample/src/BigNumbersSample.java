import java.math.BigInteger;
import java.util.LinkedList;

public class BigNumbersSample {

	public static void main(String[] args) throws InterruptedException {
		/*BigInteger myNumber = new BigInteger("10000");
		BigInteger otherNumber = new BigInteger("55");
		
		myNumber = myNumber.add(otherNumber);
		
		BigInteger bi = BigInteger.ONE;*/
		
		/*BigSamples bs = new BigSamples();
		System.out.println("1..3 = " + bs.sumTo(3) );*/
		
		//BigInteger b1 = new BigInteger("2");
		//BigInteger b2 = new BigInteger("3");
		
		// 2 < 3 (true)
		/*
		int res = b1.compareTo(b2); // -> int(-1, 0, 1);
		
		switch(res) {
		case -1:
			System.out.println("Druga liczba jest wieksza"); // druga mniejsza
			break;
		case 0:
			System.out.println("Liczby s� r�wne!");
			break;
		case 1:
			System.out.println("Druga liczba jest mniejsza!"); // pierwsza wieksza
		}
		*/
		/* Drukujemy liczby od 1 do 5 - liczby typu BigInteger */
		/*BigInteger stop = new BigInteger("5");
		for(BigInteger i = BigInteger.ONE; (i.compareTo(stop) < 1) ? true : false; i = i.add(BigInteger.ONE)) {
			System.out.println(i.toString());
		}*/
		/*BigSamples bs = new BigSamples();
		
		long start = System.currentTimeMillis(); // 1 = 1000ms
		System.out.println( bs.sumTo(new BigInteger("1"), new BigInteger("500000")).toString() );
		long stop = System.currentTimeMillis();
		System.out.println("Czas wykonania wyniosl: " + (stop - start) + "ms");
		
		long startPrimitives = System.currentTimeMillis();
		System.out.println( bs.sumTo(1, 500000) );
		long stopPrimitives = System.currentTimeMillis();
		System.out.println("Czas wykonania wyniosl: " + (stopPrimitives - startPrimitives) + "ms");*/
		
		
		/*
		Thread t = Thread.currentThread();
		System.out.println( "Aktualny w�tek nazywa si�: " + t.getName() );
		
		Thread t2 = new Thread(new SimpleThread(), "W�tek pierwszy");
		
		if( t2.isAlive() ) {
			System.out.println("W�tek �yje!");
			t2.join(1000L);
		} else {
			t2.start();
			System.out.println("W�tek nie �y�, ale ju� �yje!");
		}
		
		Thread t3 = new Thread(new SimpleThreadByInterface(4, 5), "drugi w�tek");
		t3.start();
		Thread.sleep(1000);
		if( t3.isAlive() ) {
			System.out.println("W�tek �yje!");
		}
		*/
		/*
		BigSamples bs = new BigSamples();
		System.out.println(bs.isPrime(9));
		*/
		
		Thread t1, t2;
		LinkedList<BigInteger> primes = new LinkedList<>();
		
		primes.add(new BigInteger("472882049"));
		primes.add(new BigInteger("32452867"));
		primes.add(new BigInteger("941083987"));
		primes.add(new BigInteger("3"));
		
		for(int i = 0; i < primes.size(); i += 2) {
			t1 = new Thread(new BigSamples(primes.get(i)));
			t2 = new Thread(new BigSamples(primes.get(i + 1)));
			t1.start();
			t1.setPriority(10);
			t2.start();
			t2.setPriority(Thread.NORM_PRIORITY);
		}
		System.out.println("Aktualnie dziala: " + Thread.activeCount() + " watkow.");
		
		
	}

}
