
public class SimpleThreadByInterface implements Runnable {
	
	private int a = 0;
	private int b = 0;
	
	private SimpleThreadByInterface() {
		
	}
	
	public SimpleThreadByInterface(int a, int b) {
		this.a = a;
		this.b = b;
	}
	
	@Override
	public void run() {
		System.out.println("Jestem drugim w�tkiem!");
		System.out.println("Suma: " + this.add(this.a, this.b));
	}
	
	public int add(int a, int b) {
		return a + b;
	}

}
