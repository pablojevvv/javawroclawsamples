import java.math.BigInteger;

public class BigSamples implements Runnable {
	
	private BigInteger primeToCheck;
	
	BigSamples() {}
	
	BigSamples(BigInteger prime) {
		this.primeToCheck = prime;
	}
	
	/* Metoda sumuj�ca liczby od 1..n, z tym, �e liczby s� typu BigInteger */
	public BigInteger sumTo(BigInteger n) {
		return this.sumTo(BigInteger.ONE, n);
	}
	
	public BigInteger sumTo(BigInteger start, BigInteger stop) {
		BigInteger sum = BigInteger.ZERO;
		for(BigInteger i = start; (i.compareTo(stop) < 1) ? true : false; i = i.add(BigInteger.ONE)) {
			sum = sum.add(i);
		}
		return sum;
	}
	
	public int sumTo(int start, int stop) {
		// 1. Deklaracja zmiennej sumuj�cej
		int sum = 0;
		// 2. Iteracja pomi�dzy liczb� pocz�tkow�, a liczb� ko�cow�.
		for(int i = start; i <= stop; i++) {
			// 3. Dodanie aktualnie iterowanej liczby do zmiennej sumuj�cej
			sum += i; // sum = sum + i;
		}
		
		return sum;
	}
	
	public int sumTo(int n) {
		return this.sumTo(1, n);
	}
	
	/* Napiszmy metod�, kt�ra przyjmie czas w milisekundach (ms) a zwr�ci nam String'a z reprezentacj� czasow� np.
	 * 
	 * 3min 23sec 17ms
	 * 58sec 18ms
	 * 5min 23ms
	 * 44sec 30ms
	 * 56ms
	 * 
	 *  */
	
	public String convertTime(long time) {
		String ret = "";
		long s = time / 1000;
		long m = 0;
		if(s > 60) {
			m = (long) Math.floor(s / 60);
			s = s - m * 60;
		}
		if(m > 0) ret += m + " min";
		if(s > 0) ret += s + " sec";
		else if(time > 0) ret += time + " ms";
		return ret;
	} 
	
	
	/* Metody sprawdzaj�ce czy liczba jest pierwsza. */
	public boolean isPrime(BigInteger n) {
		BigInteger i = new BigInteger("2");
		while(true) {
			if(n.mod(i).equals(BigInteger.ZERO)) {
				return false;
			}
			i = i.add(BigInteger.ONE);
			if(i.compareTo(n) == 0) {
				break;
			}
		}
		return true;
	}
	
	public boolean isPrime(int n) {
		int i = 2;
		while(i < n) {
			if(n % i == 0) {
				return false;
			}
			i++;
		}
		return true;
	}

	@Override
	public void run() {
		long start = System.currentTimeMillis();
		boolean isPrimeResult = this.isPrime(this.primeToCheck);
		long stop = System.currentTimeMillis();
		long time = stop - start;
		//System.out.println(this.primeToCheck + " jest liczba pierwsza: " + this.isPrime(this.primeToCheck));
		System.out.println(this.primeToCheck.toString() + " jest liczba pierwsza: " + isPrimeResult + "\nWywolanie zajelo: " + this.convertTime(time));
	}
}
