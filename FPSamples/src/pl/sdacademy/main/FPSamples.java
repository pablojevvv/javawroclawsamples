package pl.sdacademy.main;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import pl.sdacademy.animals.AnimalInterface;
import pl.sdacademy.animals.AnimalType;
import pl.sdacademy.animals.Cat;
import pl.sdacademy.animals.Dog;
import pl.sdacademy.calculator.Calculator;
import pl.sdacademy.cars.Car;
import pl.sdacademy.cars.Part;
import pl.sdacademy.colors.Colors;
import pl.sdacademy.greet.GreetInterface;
import pl.sdacademy.messagelog.MessageLog;

public class FPSamples {
	public static void main(String[] args) {
		/* Przyklad 1 */
		AnimalType[] arrayOfTypes = AnimalType.values();
		for(AnimalType type: arrayOfTypes) {
			System.out.println("[ " + type.name() + " ] " + type.getSlug());
		}
		
		System.out.println("-------------------");
		
		Dog d1 = new Dog("Burek", AnimalType.MAMMAL);
		Cat c1 = new Cat("Miauczek", AnimalType.MAMMAL, "green");
		
		List<AnimalInterface> animals = new LinkedList<>();
		animals.add(d1);
		animals.add(c1);
		
		for(AnimalInterface animal: animals) {
			if(animal instanceof Dog) {
				System.out.println("\tNatrafilem na psa!");
			} else if(animal instanceof Cat) {
				System.out.println("\tNatrafilem na kota!");
			}
			System.out.println("[ " + animal.getName() + " ] " + animal.getType().getSlug());
		}
		
		/* Przyklad 2 */
		System.out.println("-------------------");
		Car c = new Car();
		LinkedList<Part> parts = new LinkedList<>();
		parts.add(new Part("doors", 1999));
		parts.add(new Part("engine", 2003));
		parts.add(new Part("roof", 2007));
		
		c.setParts(parts);
		c.showCarPart(2000, "lamps");
		c.showCar();
		
		/* Przyklad 3 */
		System.out.println("-------------------");
		
		new Thread(new Runnable() {
			@Override
			public void run() {
				System.out.println("To jest wiadomosc pochodzaca z nowego watku!");
			}
		}).start();
		
		/* lambda expressions */
		new Thread(() -> {
				System.out.println("TEST");
				System.out.println("Inna wiadomosc z innego watku.");
			}).start();
		
		/* Przyklad 4 */
		System.out.println("-------------------");
		GreetInterface gi = (name) -> "Witaj " + name + "!";
		GreetInterface gi2 = new GreetInterface() {
			@Override
			public String greet(String name) {
				return "Witaj " + name + "!";
			}
		};
		System.out.println( gi.greet("Pawel") );
		System.out.println( gi2.greet("Piotr") );
		
		/* Przyklad 5 */
		System.out.println("-------------------");
		Calculator cc = new Calculator();
		
		System.out.println( "Suma 3 i 4 = " + cc.calc(3, 4, (a, b) -> a + b) );
		System.out.println( "Roznica 3 i 4 = " + cc.calc(3, 4, (a, b) -> a - b) );
		System.out.println( cc.calc(5, 6, (int a, int b) -> a + b) );
		System.out.println( cc.calc(6, 7, (a, b) -> a * b) );
		
		/* Przyklad 6 */
		System.out.println("-------------------");
		
		MessageLog ml = new MessageLog();
		ml.result("hi, am I upper case?", m -> m.toUpperCase());
		
		/* Przyklad 7 */
		System.out.println("-------------------");
		Colors colors = new Colors();
		colors.setColors(Arrays.asList("red", "blue", "green", "vanilla", "sky", "blueberry", "bronze", "yellow"));
		colors.printFiltered(p -> p.startsWith("b"));
		System.out.println("\t-------------------");
		colors.printFiltered(p -> p.length() > 4);
		System.out.println("\t-------------------");
		colors.printFiltered(p -> p.endsWith("y"));
		colors.printFiltered(p -> p.contains(""));
	}
}
