package pl.sdacademy.colors;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Predicate;

public class Colors {
	List<String> colors = new LinkedList<>();
	
	public Colors() {}

	public Colors(List<String> colors) {
		super();
		this.colors = colors;
	}

	public List<String> getColors() {
		return colors;
	}

	public void setColors(List<String> colors) {
		this.colors = colors;
	}
	
	public void printFiltered(Predicate<String> p) {
		for(String color: colors) {
			if(p.test(color)) {
				System.out.println(color);
			}
		}
	}
	
}
