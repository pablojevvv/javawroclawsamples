package pl.sdacademy.calculator;

public class Calculator {
	public int calc(int a, int b, CalculatorInterface i) {
		return i.operation(a, b);
	}
}
