package pl.sdacademy.calculator;

@FunctionalInterface
public interface CalculatorInterface {
	public int operation(int a, int b); 
}
