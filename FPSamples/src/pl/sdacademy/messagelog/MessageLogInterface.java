package pl.sdacademy.messagelog;

@FunctionalInterface
public interface MessageLogInterface {
	public String up(String message);
	
	public default void log(String message) {
		System.out.println("[ LOG ] " + message);
	}
}
