package pl.sdacademy.greet;

@FunctionalInterface
public interface GreetInterface {
	public String greet(String name);
}
