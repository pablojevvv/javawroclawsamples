package pl.sdacademy.cars;

public interface CarInterface {
	default public void showCarPart(int year, String partName) {
		System.out.println("\t[ " + year + " ] " + partName);
	}
	
}
