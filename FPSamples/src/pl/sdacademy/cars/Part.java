package pl.sdacademy.cars;

public class Part {
	private String name;
	private int productionYear;
	
	public Part() {}

	public Part(String name, int productionYear) {
		super();
		this.name = name;
		this.productionYear = productionYear;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getProductionYear() {
		return productionYear;
	}

	public void setProductionYear(int productionYear) {
		this.productionYear = productionYear;
	}
	
	
}
