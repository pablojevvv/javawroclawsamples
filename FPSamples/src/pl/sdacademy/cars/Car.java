package pl.sdacademy.cars;

import java.util.LinkedList;
import java.util.List;

public class Car implements CarInterface {
	private String name;
	private List<Part> parts = new LinkedList<>();
	
	public Car() {}

	public Car(String name, List<Part> parts) {
		super();
		this.name = name;
		this.parts = parts;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Part> getParts() {
		return parts;
	}

	public void setParts(List<Part> parts) {
		this.parts = parts;
	}
	
	public void showCar() {
		for(Part p: parts) {
			this.showCarPart(p.getProductionYear(), p.getName());
		}
	}
	
	
	
}
