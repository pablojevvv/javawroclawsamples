package pl.sdacademy.animals;

public enum AnimalType {
	FISH("Ryba"),
	MAMMAL("Ssak"),
	REPTILE("Gad"),
	BIRD("Ptak");
	
	private String slug;
	
	AnimalType(String slug) {
		this.slug = slug;
	}
	
	public String getSlug() {
		return slug;
	}
}
