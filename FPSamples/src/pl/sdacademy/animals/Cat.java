package pl.sdacademy.animals;

public class Cat implements AnimalInterface {
	private String name;
	private AnimalType type;
	private String eyesColor;
	
	public Cat() {}

	public Cat(String name, AnimalType type, String eyesColor) {
		super();
		this.name = name;
		this.type = type;
		this.eyesColor = eyesColor;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public AnimalType getType() {
		return type;
	}

	public String getEyesColor() {
		return eyesColor;
	}

	public void setEyesColor(String eyesColor) {
		this.eyesColor = eyesColor;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setType(AnimalType type) {
		this.type = type;
	}
	
	
	
	
}
