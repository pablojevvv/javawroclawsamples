package pl.sdacademy.animals;

public class Dog implements AnimalInterface {
	
	private String name;
	private AnimalType type;
	
	public Dog() {}

	public Dog(String name, AnimalType type) {
		super();
		this.name = name;
		this.type = type;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public AnimalType getType() {
		return type;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setType(AnimalType type) {
		this.type = type;
	}

	
}
