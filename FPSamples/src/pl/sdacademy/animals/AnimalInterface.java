package pl.sdacademy.animals;

public interface AnimalInterface {
	public String getName();
	public AnimalType getType();
}
